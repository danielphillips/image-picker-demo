//
//  DPPopoverMainViewController.m
//  Image Picker
//
//  Created by Daniel Phillips on 12/04/2013.
//  Copyright (c) 2013 Daniel Phillips. All rights reserved.
//

#import "DPPopoverMainViewController.h"

@interface DPPopoverMainViewController()<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) UIViewController *oneController;
@property (nonatomic, strong) UIViewController *twoController;
@property (nonatomic, strong) UIImagePickerController *mediaController;
@property (nonatomic, weak, readwrite) UIViewController *visibleTableViewController;

@end

@implementation DPPopoverMainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    UISegmentedControl *formatTypeChooser = [[UISegmentedControl alloc] initWithItems:@[ @"One", @"Two", @"Media" ]];
    formatTypeChooser.segmentedControlStyle = UISegmentedControlStyleBar;

    formatTypeChooser.frame = CGRectMake(0.0, 0.0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(formatTypeChooser.bounds));
    formatTypeChooser.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    formatTypeChooser.selectedSegmentIndex = 0;
    [formatTypeChooser addTarget:self action:@selector(_formatTypeChooserValueChanged:) forControlEvents:UIControlEventValueChanged];
    self.navigationItem.titleView = formatTypeChooser;
    
    [self _formatTypeChooserValueChanged:nil];
}

#pragma mark - Segmented Control Methods
- (void)_formatTypeChooserValueChanged:(UISegmentedControl *)control
{
    UIViewController *newViewController = nil;
    
    NSUInteger selectedIndex = control ? control.selectedSegmentIndex : 0;
    
    switch (selectedIndex) {
        case 0:
        {
            // One
            if( !self.oneController ){
                self.oneController = [[UIViewController alloc] init];
                self.oneController.view.backgroundColor = [UIColor redColor];
                self.oneController.contentSizeForViewInPopover = CGSizeMake(320.0, 320.0);
            }
            
            newViewController = self.oneController;
        }
            break;
        case 1:
        {
            // Two
            if( !self.twoController ){
                self.twoController = [[UIViewController alloc] init];
                self.twoController.view.backgroundColor = [UIColor greenColor];
                self.twoController.contentSizeForViewInPopover = CGSizeMake(320.0, 480.0);
            }
            
            newViewController = self.twoController;
        }
            break;
        case 2:
        {
            // Media
            if( !self.mediaController ){
                self.mediaController = [[UIImagePickerController alloc] init];
                self.mediaController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                self.mediaController.delegate = self;
            }
            
            newViewController = self.mediaController;
        }
            break;
        default:
            break;
    }
    
    if (!newViewController)
        return;
    
    // remove old one
    [self.visibleTableViewController.view removeFromSuperview];
    [self.visibleTableViewController willMoveToParentViewController:nil];
    [self.visibleTableViewController removeFromParentViewController];

    // change the controller
    self.visibleTableViewController = newViewController;
        
    // add new one
    [self addChildViewController:self.visibleTableViewController];
    self.visibleTableViewController.view.frame = CGRectMake(0.0, 0.0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
    self.visibleTableViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.visibleTableViewController.view];
    [self.visibleTableViewController didMoveToParentViewController:self];
    
    self.contentSizeForViewInPopover = self.visibleTableViewController.contentSizeForViewInPopover;
}

#pragma mark - Image Picker Hack
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if(![navigationController isKindOfClass:[UIImagePickerController class]])
        return;
    
    UINavigationItem *item = [viewController navigationItem];
    
    if(!item.rightBarButtonItems){
        [item setValue:nil forKey:@"_customRightViews"];
    }else{
        [item setRightBarButtonItems:nil];
    }
}

@end
