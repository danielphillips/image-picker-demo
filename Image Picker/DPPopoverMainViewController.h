//
//  DPPopoverMainViewController.h
//  Image Picker
//
//  Created by Daniel Phillips on 12/04/2013.
//  Copyright (c) 2013 Daniel Phillips. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DPPopoverMainViewController : UIViewController

@property (nonatomic, weak, readonly) UIViewController *visibleTableViewController;

@end
