//
//  AppDelegate.h
//  Image Picker
//
//  Created by Daniel Phillips on 26/05/2013.
//  Copyright (c) 2013 Daniel Phillips. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
